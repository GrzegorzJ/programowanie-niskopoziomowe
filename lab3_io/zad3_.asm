%include "asm64_io.inc"

section .data
;
	arr1 times 5 db 0
	arr2 times 5 db 0
	arr3 times 5 db 0
;

section .bss
;
; dane niezainicjalizowane
;

section .text
global asm_main
asm_main:
enter 0,0 

; ---- start
mov eax, 0
mov	ecx, 0					
petla_for: 				;read first sequence
	cmp	    ecx, 5			
	jge	    koniec_petli
	
	call read_char
	sub eax, '0'		;change char to number
	mov [arr1 + ecx], al ;save into array
	
	inc	    ecx		        
	jmp	    petla_for
koniec_petli:

call read_char

mov eax, 0
mov	ecx, 0					
petla_for2: 			;read second sequence
	cmp	    ecx, 5			
	jge	    koniec_petli2
	
	call read_char
	sub eax, '0'
	mov [arr2 + ecx], al 
	
	inc	    ecx		        
	jmp	    petla_for2
koniec_petli2:

mov eax, 0
mov	ecx, 4					
mov ebx, 0
petla_for_sum:			;sum two sequences, start from back
	cmp	    ecx, 0
	jl	    koniec_petli_sum
	mov eax, 0

	mov al, [arr1 + ecx]
	add al, [arr2 + ecx]
	add al, bl 			;add shift
	mov bl, 0

	cmp al, 9			;check if number consist of only one sign
	jle next
	sub al, 10	
	mov bl, 1 			;save shift

	next:
	add al, '0' 		;change to ASCII

	mov [arr3 + ecx], al ;save to result array
	
	dec	    ecx		       
	jmp	    petla_for_sum
koniec_petli_sum:


mov eax, 0
mov	ecx, 0					
petla_for_print: 		;print results
	cmp	    ecx, 5			
	jge	    koniec_petli_print

	mov al, [arr3 + ecx]
	call print_char
	
	inc	    ecx		        
	jmp	    petla_for_print
koniec_petli_print:

; --- end

koniec_programu:
mov rax, 0
leave
ret

