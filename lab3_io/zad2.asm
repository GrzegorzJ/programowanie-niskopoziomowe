%include "asm64_io.inc"

section .data

	array times 6 db 0
	array_size dd 6

section .bss


section .text

global asm_main
asm_main:
enter 0,0 


mov eax, 0
mov	ecx, 0					
fill_array_loop: 
	cmp	ecx, [array_size]			
	jge end_fill_array_loop
	 
	call read_char
			
	cmp al, '*' 
	je end_fill_array_loop
		
	mov [array + ecx], al
	
	inc	ecx		        
	jmp	fill_array_loop
end_fill_array_loop:

dec ecx


print_in_reverse_order_loop: 
	cmp ecx, 0
	jl end_print_in_reverse_order_loop	
				
	mov al, [array + ecx]
	cmp al, 97 ;duzych liter nie konwertujemy
	jl not_convert_to_upper	
	sub al, 32
not_convert_to_upper:
	call print_char

	dec ecx	 		       
	jmp	print_in_reverse_order_loop
end_print_in_reverse_order_loop:


call print_nl

mov rax, 0 
leave
ret

