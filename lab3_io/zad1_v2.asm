%include "asm64_io.inc"

section .data

	array times 4 db 0
	array_size db 4

section .bss


section .text

global asm_main
asm_main:
enter 0,0 


mov eax, 0
mov	ecx, 0				
	
fill_array_loop: 
	cmp	ecx, [array_size]			
	jge	end_fill_array_loop
	
	call read_int
	mov [array + ecx], al
	
	inc	ecx		        
	jmp	fill_array_loop
end_fill_array_loop:

mov eax, 0
mov	ecx, 0					
mov ebx, 0
add_items_loop: 
	cmp	    ecx, [array_size]			
	jge	    end_add_items_loop	
	add bl, [array + ecx]
	
	inc	    ecx		       
	jmp	    add_items_loop
end_add_items_loop:

mov eax, ebx
call print_int


mov rax, 0 
leave
ret

