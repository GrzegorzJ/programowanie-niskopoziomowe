%include "asm64_io.inc"

section .data

	num1_arr times 6 db 0
	num1_arr_size dd 6

	num2_arr times 6 db 0
	num2_arr_size dd 6

section .bss


section .text

global asm_main
asm_main:
enter 0,0 


mov eax, 0
mov	ecx, 0					
fill_array1_loop: 
	cmp	ecx, [num1_arr_size]			
	jge end_fill_array1_loop
	 
	call read_char
		
	mov [num1_arr + ecx], al
	
	inc	ecx		        	
	jmp	fill_array1_loop

end_fill_array1_loop:

mov ecx, 0
fill_array2_loop: 
	cmp	ecx, [num2_arr_size]			
	jge end_fill_array2_loop
	 
	call read_char
		
	mov [num2_arr + ecx], al
	
	inc	ecx		        	
	jmp	fill_array2_loop

end_fill_array2_loop:

mov ecx, 0  
mov edi, 0 ; przniesienie
sum_loop: 
	cmp ecx, 5
	jge end_sum_loop	
				
	mov eax, [num1_arr + ecx]
	sub eax, '0' 
	
	mov ebx, [num2_arr + ecx]
	sub ebx, '0' 

	add eax, ebx

	cmp eax, 9
	jg no_carriage
	;mov edi, 1
no_carriage:
	;add eax, edi ;jesli jest to dodaj przeniesienie
	add eax, '0'
	call print_char 	

	inc ecx
	mov edi, 0	
	jmp sum_loop	
end_sum_loop:


mov eax, edx

mov rax, 0 
leave
ret

