%include "asm64_io.inc"

section .data

	array times 100 db 0 ;zakladamy, ze nie wczytujemy wiecej niz  100 znakow
	array_size dd 20
	
	s1 db "abcdefghijklmnopqrstuvwxyz", 0ah
	s2 db "zyxwvutsrqponmlkjihgfedcba", 0ah
section .bss


section .text

global asm_main
	asm_main:

enter 0,0 

;wypelniamy talblice podanymi literami
mov eax, 0
mov	ecx, 0					
fill_array_loop: 
	cmp	ecx, [array_size]			
	jge end_fill_array_loop
	 
	call read_char
	
	;cmp al, ''
	;je end_fill_array_loop					
		
	cmp al, '*' 
	je end_fill_array_loop
	
	mov [array + ecx], al
	
	inc	ecx		        
	jmp	fill_array_loop
end_fill_array_loop:



mov edi, ecx
mov ecx, 0
;iterujemy po elementach tablicy zamieniajac elementy z s1 na s2 jesli nie litery(<58)
translate_words_loop: 
	cmp	ecx, edi
	jg end_translate_words_loop
						
	mov al, [array + ecx]
	cmp al, 57 ;zakladajac, ze tablica ma tylko litery i cyfry <58 oznacza cyfre, ktora pomijamy
	jle not_translate_character
	
	mov edx, 0
;szukamy w ciagu s1 indeksu na ktorym jest obecny element a nastepenie wyciagamy element z s2
;np jesli dana litera to 'c' to szukamy w s1 na ktorym miejscu jest 'c' czyli tu na 3 i bierzemy numer indeksu z przesunieciem 3 z s2 czyli 'x'
find_s1_occurence:
	cmp al, [s1 + edx]
	je end_find_s1_occurence
	inc edx
	jmp find_s1_occurence	
	
end_find_s1_occurence:		
	mov al, [s2 + edx]

not_translate_character:
	call print_char

	inc ecx	 		       
	jmp	translate_words_loop
end_translate_words_loop:

call print_nl

mov rax, 0 
leave
ret

