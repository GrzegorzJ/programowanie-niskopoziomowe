%include "asm64_io.inc"

section .data
;
	arr times 3 db 0
;

section .bss
;
; dane niezainicjalizowane
;

section .text
global asm_main
asm_main:
enter 0,0 

; ---- start
mov eax, 0
mov	ecx, 0					
petla_for: 
	cmp	    ecx, 3			
	jge	    koniec_petli
	; mov eax, ecx
	call read_int
	mov [arr + ecx], al
	
	inc	    ecx		        
	jmp	    petla_for
koniec_petli:

mov eax, 0
mov	ecx, 0					
mov ebx, 0
petla_for_sum: 
	cmp	    ecx, 3			
	jge	    koniec_petli_sum	
	add bl, [arr + ecx]
	
	inc	    ecx		       
	jmp	    petla_for_sum
koniec_petli_sum:

mov eax, ebx
call print_int

; --- end

koniec_programu:
mov rax, 0 ; kod zwracany z funkcji
leave
ret

