#!/bin/bash
clear
echo "Compile, link and run:" $1
echo "#####################"

g++ -m32 -o main$1.o -c main$1.cpp
nasm -felf32 -o zad$1.o zad$1.asm
g++ -m32 main$1.o zad$1.o -o zad$1
./zad$1

#sample run command
#./compile_and_run.sh zad1