BITS 32
section .data

section .text 

global iloczyn_skalarny        ;KONWENCJA!!! - funkcja suma ma byÄ widziana w innych moduĹach aplikacji
 
iloczyn_skalarny:         

%idefine    n dword [ebp+8]
%idefine    x dword [ebp+12]
%idefine    y dword [ebp+16]

; tu zaczyna siÄ wĹaĹciwy kod funkcji
enter 0,0
push eax
push ebx
push ecx
push edx

mov ecx, 0
mov eax, x
mov edx, y
fldz 		;push zero

petla:
	cmp ecx, n
	jge koniec_petli

	fld tword [eax]		;sum x1
	add eax, 12
	fld tword [edx]		;sum x1 x2
	add edx, 12

	fmulp 				;sum x1x2
	faddp 				;sum+x1x2


	inc ecx
	jmp petla
koniec_petli:

pop edx
pop ecx
pop ebx
pop eax
; tu koĹczy siÄ wĹaĹciwy kod funkcji

leave                            ; LEAVE = mov esp, ebp / pop ebp - konwencjonalne zakoĹczenie procedury
ret