// KOMPILACJA - kod źródłowy C w main.c, kod źródłowy ASM w suma.asm
// LINUX :
// jak sa problemy z kompilacja c na linuxie to:
// zainstalowac http://stackoverflow.com/questions/23498237/compile-program-for-32bit-on-64bit-linux-os-causes-fatal-error
// nasm -felf32 suma.asm -o suma.o
// gcc -m32 -o main.o -c main.c
// gcc -m32 main.o suma.o -o suma
// 
// WINDOWS :
// nasm -fcoff suma.asm -o suma.obj
// gcc -o main.obj -c main.c
// gcc main.obj suma.obj -o suma.exe
#include <iostream>
#include <math.h>

extern "C" void tablicuj(double a, double b, double P, double Q, double xmin, double xmax, int k,  double * wartosci);

void test(double a, double b, double P, double Q, double xmin, double xmax, int k,  double * wartosci) {
	int i;
	double step = (xmax - xmin) / k;
	double x;

	for(i = 0; i < k; i++) {
		//y=a*(sin(P*pi*x/180.0))^2 + b*(sin(Q*pi*x/180.0))^2
		x = xmin + i * step;
		double y = a * sqrt(sin(P*M_PI*x/180.0)) + b * sqrt(sin(Q*M_PI*x/180.0));
		wartosci[i] = y;
	}
}

int main()
{
 	double res [10];
 	double res2 [10];

 	double a = 1.;
 	double b = 1.;
 	double P = 1.;
 	double Q = 1.;
 	double xmin = 1.;
 	double xmax = 10.;
 	int k = 5;

 	tablicuj(a,b,P,Q,xmin,xmax,k,res);
 	test(a,b,P,Q,xmin,xmax,k,res2);
	std::cout << res[0] << " " << res[1] << " " << res[2] << "\n";
	std::cout << res2[0] << " " << res2[1] << " " << res2[2] << "\n";

	return 0;
}
/* Wyjście:
 * 14
 * 2
 */ 