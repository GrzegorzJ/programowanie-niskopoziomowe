BITS 32
section .bss
	pi_P_2 rest 1
	pi_Q_2 rest 1
	step rest 1
	x rest 1
	i resd 1

section .data
	dwa dw 2

section .text 

global tablicuj        ;KONWENCJA!!! - funkcja suma ma byĂÂ widziana w innych moduÄšÂach aplikacji
 
tablicuj:     
push ebp
mov ebp, esp    

%idefine    a qword [ebp+8]
%idefine    b qword [ebp+16]
%idefine    P qword [ebp+24]
%idefine    Q qword [ebp+32]
%idefine    xmin qword [ebp+40]
%idefine    xmax qword [ebp+48]
%idefine    k dword [ebp+56]
%idefine    wartosci dword [ebp+60]

;y=a*(sin(P*2*pi*x))^2 + b*(sin(Q*2*pi*x))^2


push eax
push ebx
push ecx
push edx

;step = (xmax - xmin) / (k-1)
;x = xmin + i * step

fld P 		;P
fild dword [dwa] ;P 2
fmulp 		;P*2
fldpi 		;P*2 pi 
fmulp 		;P*2*pi

mov eax, pi_P_2
fstp tword [eax]

fld Q 		;Q
fild dword [dwa] ;Q 2
fmulp 		;Q*2
fldpi 		;Q*2 pi 
fmulp 		;Q*2*pi

mov eax, pi_Q_2
fstp tword [eax]

fld xmax 		;xmax
fld xmin		;xmax xmin
fsubp 			;xmax-xmin
fild k 			;xmax-xmin k
fld1 			;xmax-xmin k 1
fsubp 			;xmax-xmin k-1
fdivp 			;(xmax-xmin)/(k-1) 

mov eax, step
fstp tword [eax]

mov ebx, wartosci
mov ecx, 0
petla:
 	cmp ecx, k
 	jge koniec_petli

 	fld tword [step] 	;step 
 	mov [i], ecx
 	fild dword [i] 		;step i
 	fmulp 				;step*i
 	fld xmin 			;step*i xmin
 	faddp 				;step*i+xmin 
 	mov eax, x 
	fstp tword [eax] 	;x = step*i+xmin 


	fld a 
	fld tword [pi_P_2]
	fld tword [x] 
	fmulp 
	fsin 
	fmul st0, st0 
	fmulp 

	; count second element and add to first
	fld b 
	fld tword [pi_Q_2]
	fld tword [x] 
	fmulp 
	fsin 
	fmul st0, st0 
	fmulp 

	faddp 

	fstp qword [ebx]
	add ebx, 8


 	inc ecx
 	jmp petla
 koniec_petli:

pop edx
pop ecx
pop ebx
pop eax


pop ebp                          ; LEAVE = mov esp, ebp / pop ebp - konwencjonalne zakoÄšÂczenie procedury
ret