BITS 32
section .text 

global wartosc        ;KONWENCJA!!! - funkcja suma ma być widziana w innych modułach aplikacji
                   ; pod Windowsem należy dodać podkreślenie przed suma
 
wartosc:         

%idefine    a qword [ebp+8]
%idefine    b qword [ebp+16]
%idefine    c qword [ebp+24]
%idefine    d qword [ebp+32]
%idefine    x qword [ebp+40]

; tu zaczyna się właściwy kod funkcji
enter 0,0
fld a 		;a
fld x 		;a x
fmulp st1 	;ax
fld b 		;ax b
faddp st1 	;ax+b
fld x 		;ax+b x
fmulp st1 	;(ax+b)x
fld c 		;(ax+b)x c
faddp st1 	;(ax+b)x+c
fld x		;(ax+b)x+c x
fmulp st1   ;((ax+b)x+c)x
fld d		;((ax+b)x+c)x d
faddp st1 	;((ax+b)x+c)x+d

;fmul st1  	;cx c

; tu kończy się właściwy kod funkcji

leave                            ; LEAVE = mov esp, ebp / pop ebp - konwencjonalne zakończenie procedury
ret