BITS 32
section .data
	dwa dw 2

section .text 

global prostopadloscian        ;KONWENCJA!!! - funkcja suma ma być widziana w innych modułach aplikacji
                   ; pod Windowsem należy dodać podkreślenie przed suma
 
prostopadloscian:         

%idefine    a dword [ebp+8]
%idefine    b dword [ebp+12]
%idefine    c dword [ebp+16]
%idefine    obj dword [ebp+20]
%idefine    pole dword [ebp+24]

; tu zaczyna się właściwy kod funkcji
enter 0,0	
push ebx	
	
fld a 		;a
fld b 		;a b
fmulp	 	;ab    		;st1 by default?
fld c 		;ab c
fmulp st1 	;abc
mov ebx, obj
fstp dword [ebx]

fld a 		;a
fld b 		;a b
faddp st1 	;a+b
fld c		;a+b c
fmulp st1 	;(ac+bc)
fld a 		;(ac+bc) a
fld b 		;(ac+bc) a b
fmulp		;(ac+bc) ab
faddp 		;(ac+bc+ab)
fild dword [dwa] ;(ac+bc+ab) 2
fmulp 		;2(ac+bc+ab)

mov ebx, pole
fstp dword [ebx]

;fmul st1  	;cx c

; tu kończy się właściwy kod funkcji
pop ebx
leave                            ; LEAVE = mov esp, ebp / pop ebp - konwencjonalne zakończenie procedury
ret