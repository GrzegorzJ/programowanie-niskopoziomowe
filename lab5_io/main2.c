
#include <stdio.h>

int suma (int n, int * array);         /* prototyp funkcji */

int main()
{
    int n = 5;
    int arr[5] = {1, 2, 10, 4, 5};
    int i = 0;

    for (i = 0; i < n; i++ ){
       printf("%d : %d\n",  i, *(arr + i) );
   	}

    printf("sum of %d elements vector is: ", n);
    printf("%d\n", suma(n, arr));
    return 0;
}