
#include <stdio.h>

typedef struct{
    int min;
    int max;
} MM;

MM minmax( int N, ...);

int main(){
   MM wynik = minmax(5, 1, -6, 4 , 90, 4);
   printf("min = %d, max = %d \n", wynik.min, wynik.max);
   return 0;
}