BITS 32
section .bss
	ebx_copy resd 1
	edi_copy resd 1

section .text 

global minmax        ;KONWENCJA!!! - funkcja suma ma być widziana w innych modułach aplikacji
                   ; pod Windowsem należy dodać podkreślenie przed suma
 
minmax:
push   ebp                    ;KONWENCJA!!! - tworzymy ramkę stosu na początku funkcji
mov    ebp, esp            

; po wykonaniu push ebp i mov ebp, esp:
; w [ebp]    znajduje się stary EBP
; w [ebp+4]  znajduje się adres powrotny z procedury
; w [ebp+8]  znajduje się pierwszy parametr,
; w [ebp+12] znajduje się drugi parametr
; itd.

%idefine    n    [ebp+12]

; tu zaczyna się właściwy kod funkcji
mov [ebx_copy], ebx
mov [edi_copy], edi


mov ecx, n
mov eax, 999999 ;min
mov edx, 0 		;max

min_loop:
	cmp ecx, 0
	jle end_min_loop
	dec ecx	

	mov ebx, [ebp + 12 + 4 * ecx]
	cmp eax, ebx
	jle max_check
	mov eax, ebx

	max_check:
	cmp edx, ebx
	jge min_loop
	mov edx, ebx

	jmp min_loop
end_min_loop:

; save result
mov edi, [ebp + 8]		;result address
mov [edi], eax
mov [edi + 4], edx

; mov eax, edi
;result in eax register

mov ebx, [ebx_copy]
mov edi, [edi_copy]
; tu kończy się właściwy kod funkcji

leave                            ; LEAVE = mov esp, ebp / pop ebp - konwencjonalne zakończenie procedury
ret