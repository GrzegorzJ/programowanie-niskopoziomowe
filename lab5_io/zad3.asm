BITS 32
section .text 

global sortuj        ;KONWENCJA!!! - funkcja suma ma być widziana w innych modułach aplikacji
                   ; pod Windowsem należy dodać podkreślenie przed suma
 
sortuj:
push   ebp                    ;KONWENCJA!!! - tworzymy ramkę stosu na początku funkcji
mov    ebp, esp            

; po wykonaniu push ebp i mov ebp, esp:
; w [ebp]    znajduje się stary EBP
; w [ebp+4]  znajduje się adres powrotny z procedury
; w [ebp+8]  znajduje się pierwszy parametr,
; w [ebp+12] znajduje się drugi parametr
; itd.

%idefine    a    [ebp+8]
%idefine    b    [ebp+12]
%idefine    c    [ebp+16]

; tu zaczyna się właściwy kod funkcji
; [adres] odwolanie do miejsca adresu w pamieci
push ebx 		;a = 5
push esi		;b = 4
push edi		;c = 3


mov eax, a		;ecx <- &a 
mov ebx, b
mov ecx, c

mov edx, [ebx]
cmp edx, [ecx]
jl next
;switch b,c
mov edx, [ebx]
mov esi, [ecx]
mov [ebx], esi 
mov [ecx], edx
next:

mov edx, [eax]
cmp edx, [ebx]
jl next2
;switch a,b
mov edx, [eax]
mov esi, [ebx]
mov [eax], esi 
mov [ebx], edx
next2:

mov edx, [ebx]
cmp edx, [ecx]
jl next3
;switch b,c
mov edx, [ebx]
mov esi, [ecx]
mov [ebx], esi 
mov [ecx], edx
next3:
	

mov eax, 0
pop edi
pop esi
pop ebx
;result in eax register

; tu kończy się właściwy kod funkcji

leave                            ; LEAVE = mov esp, ebp / pop ebp - konwencjonalne zakończenie procedury
ret