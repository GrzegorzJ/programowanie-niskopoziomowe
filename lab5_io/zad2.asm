BITS 32
section .text 

global suma        ;KONWENCJA!!! - funkcja suma ma być widziana w innych modułach aplikacji
                   ; pod Windowsem należy dodać podkreślenie przed suma
 
suma:
push   ebp                    ;KONWENCJA!!! - tworzymy ramkę stosu na początku funkcji
mov    ebp, esp            

; po wykonaniu push ebp i mov ebp, esp:
; w [ebp]    znajduje się stary EBP
; w [ebp+4]  znajduje się adres powrotny z procedury
; w [ebp+8]  znajduje się pierwszy parametr,
; w [ebp+12] znajduje się drugi parametr
; itd.

%idefine    n    [ebp+8]
%idefine    arr    [ebp+12]

; tu zaczyna się właściwy kod funkcji

mov ecx, n
mov edx, arr
mov eax, 0

sum_loop:
	cmp ecx, 0
	jle end_sum_loop
	dec ecx	

	add eax, [edx + 4 * ecx]
	jmp sum_loop
end_sum_loop:


;result in eax register

; tu kończy się właściwy kod funkcji

leave                            ; LEAVE = mov esp, ebp / pop ebp - konwencjonalne zakończenie procedury
ret