
#include <stdio.h>

void sortuj( int * a, int * b, int * c);

int main()
{
    int a;
    int b;
    int c;
    
    scanf("%d %d %d", &a, &b, &c);

    printf("elements: %d, %d, %d \n", a, b, c);
    sortuj(&a, &b, &c);
    printf("sorted: %d, %d, %d \n", a, b, c);

    return 0;
}