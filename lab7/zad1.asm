;Proszę napisać w assemblerze funkcję o nagłówku
;extern "C" double wartosc(double a, double b, double  c, double d, double x);

;wyliczającą wartość wyrażenia y=ax3+bx2+cx+d. Funkcja ma pobierać dane wejściowe od procedury wołającej napisanej w C, która wyświetla wyniki ;obliczeń.


%define a qword [ebp+8]
%define b qword [ebp+16]
%define c qword [ebp+24]
%define d qword [ebp+32]
%define x qword [ebp+40]



global wartosc

;y=ax3+bx2+cx+d
wartosc:
	enter 0, 0
		fld x ; x
		fld x ; x x
		fld x ; x x x
		fmulp st1 ; x * x, x
		fmulp st1 ; x * x * x
		fld a ; a, 3x
		fmulp st1 ; ax^3
		
		fld x ; x, ax^3
		fld x ; x, x, ax^3
		fmulp st1 ; x^2, ax^3
		fld b ;b, x^2, ax^3	
		fmulp st1 ; bx^2, ax^3
		faddp st1; bx^2 + ax^3

		fld c
		fld x
		fmulp st1 
		faddp st1 ; cx + bx^2 + ax^3
		
		fld d
		faddp st1 ; d + cx + bx^2 + ax^3
	leave 
	ret


