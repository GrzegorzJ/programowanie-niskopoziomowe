;Proszę napisać funkcję asemblerową o nagłówku
;extern "C" long double iloczyn_skalarny(int n, long double * x, long double * y);
;mnożącą skalarnie dwa n-wymiarowe wektory liczb rzeczywistych o współrzędnych w tablicach x i y. 
;Funkcja ma pobierać dane wejściowe od procedury wołającej napisanej w C, która wyświetla wyniki obliczeń.

%define n dword [ebp+8]
%define x dword [ebp+12]
%define y dword [ebp+16]


global iloczyn_skalarny


segment .data
zero dw 0	
	
iloczyn_skalarny:

enter 0, 0


fild word [zero]	
mov eax, x
mov ebx, y 
mov ecx, n

sum_loop:
	cmp ecx, 0
	je end_sum_loop
	
		fld tword [eax]		
		fld tword [ebx]		
		fmulp st1
		faddp st1

		add eax, 12
		add ebx, 12
		dec ecx
		jmp sum_loop
 		
end_sum_loop:

leave 
ret


