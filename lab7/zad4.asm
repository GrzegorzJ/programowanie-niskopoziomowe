/*Proszę napisać w assemblerze funkcję o nagłówku

extern "C" void tablicuj(double a, double b, double P, double Q, double xmin, double xmax, int k,  double * wartosci);
tablicującą wartości funkcji:
y=a*(sin(P*2*pi*x))2 + b*(sin(Q*2*pi*x))2
dla k>=2 równoodległych punktów w przedziale od xmin do xmax.(tj. x1 = xmin ... xk = xmax ).
Wynik ma być zapisany w tablicy wartosci (zakladamy, że jest odpowiednio duza).*/

#void tablicuj(double a, double b, double P, double Q, double xmin, double xmax, int k,  double * wartosci);

%define a qword [ebp+8]
%define b qword [ebp+16]
%define P qword [ebp+24]
%define Q qword [ebp+32]
%define xmin qword [ebp+40]
%define xmax qword [ebp+48]
%define k dword [ebp+52]
%define wartosci dword [ebp+56]

global tablicuj

segment .data

dwa dw 2
step rest 1	
tablicuj:

enter 0, 0
	
mov ecx, k ; wielkosc tablicy
dec ecx
mov eax, wartosc

;step size (xmax-xmin)/(k-1)
fld 1
fld k
fsubp st1
fld xmin
fld xmax
fsubp st1
fdivp st1
mov ebx, step
fstp tword [ebx]



; y=a*(sin(P*2*pi*x))^2 + b*(sin(Q*2*pi*x))^2
sum_loop:
	cmp ecx, 0
	jl end_sum_loop
	
		fld P		
		fld dword [2_pi]
		fld dword [wartosc + ecx]		
		fmulp st1
		fmulp st1 ;P*2*pi*x)

		fsin ;sin(P*2*pi*x)

		fmulp ;(sin(P*2*pi*x))^2
		fld a		
		fmulp st1 ;a*(sin(P*2*pi*x))^2

		
		fld dword [2_pi]
		fld dword [wartosc + ecx]
		fld Q
		fmulp st1
		fmulp st1
		fsin 
		fld b
		fmulp st1

		faddp st1

				
		add eax, 12
		add ebx, 12
		dec ecx
		jmp sum_loop
 		
end_sum_loop:

leave 
ret
