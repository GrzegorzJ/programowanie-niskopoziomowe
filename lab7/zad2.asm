;Proszę napisać funkcję asemblerową o nagłówku
;extern "C" void prostopadloscian( float a, float b, float c, float * objetosc, float * pole);
;wyliczającą objętość i pole powierzchni prostopadłościanu a. Funkcja ma pobierać dane wejściowe od procedury wołającej napisanej w C, która ;wyświetla wyniki obliczeń.

%define a dword [ebp+8]
%define b dword [ebp+12]
%define c dword [ebp+16]
%define obj dword [ebp+20]
%define pol dword [ebp+24]

segment .data
two dw 2

global prostopadloscian


prostopadloscian:
	enter 0, 0
		; objetosc		
		fld a ; a
		fld b ; b a
		fld c ; c b a
		fmulp st1 ; c * b, a
		fmulp st1 ; c * b * a

		mov ecx, obj ;ładuję adres obj do rejestru
		fstp dword [ecx]; ściągam ze stosu do *obj		
		
		;pole
		fld a 
		fld b 
		fmulp st1 ; a*b 

		fld a 
		fld c 
		fmulp st1 ; a*c 
		
		fld b 
		fld c 
		fmulp st1 ; b*c 		

		faddp st1
		faddp st1
		
		;(ab + bc + ac)
		fild word [two]
		fmulp st1
		
		mov ecx, pol ;ładuję adres pol do rejestru
		fstp dword [ecx] ; ściągam ze stosu do *pol
		
	leave 
	ret
