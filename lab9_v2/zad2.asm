;RDI, RSI, RDX, RCX, R8, R9 (pierwsze sześć argumentów, które są liczbami całkowitymi lub wskaźnikami)
;XMM0, XMM1, ..., XMM7 (Agumenty zmiennoprzecinkowe (poza long double) są umieszczane w rejestrach SSE:)
;Napisz w assemblerze funkcję 
;extern "C" double wartosc (double a, double b, double x);
;która zwraca wartosc = a*x+b 
;Wywołanie wartosc(4, 3, 2); powinno zwrócić 11.   

global wartosc
wartosc: 
	push rbp           ; zachowujemy starÄ… wartoÅ›Ä‡ ebp
	mov rbp, rsp     
	
	mulsd xmm0, xmm2
	addsd xmm0, xmm1	

	mov rsp, rbp       ; zwolnienie pamiÄ™ci na stosie
	pop rbp            ; przywrÃ³cenie poprzedniej wartoÅ›ci ebp
	ret
