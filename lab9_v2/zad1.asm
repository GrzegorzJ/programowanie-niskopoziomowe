;RDI, RSI, RDX, RCX, R8, R9 (pierwsze sześć argumentów, które są liczbami całkowitymi lub wskaźnikami)
;XMM0, XMM1, ..., XMM7 (Agumenty zmiennoprzecinkowe (poza long double) są umieszczane w rejestrach SSE:)

;Napisz w assemblerze funkcję 
;extern "C" int iloczyn (int n, int * tab);
;która oblicza iloczyn n liczb całkowitych znajdujących się w tablicy tab np. 
;int tab[] ={1, 2, 3, 4};
;iloczyn(4, tab);
;powinno zwrócić 24. 

global iloczyn
iloczyn: 
	push rbp           ; zachowujemy starÄ… wartoÅ›Ä‡ ebp
	mov rbp, rsp     
	
	mov rax, 1
	mov rcx, 0

	mul_loop:
		cmp rcx, rdi
		je end_mul_loop 
		mov rdx, [rsi + 4 * rcx]
		mul rdx
		inc rcx
		jmp mul_loop
	end_mul_loop:

	mov rsp, rbp       ; zwolnienie pamiÄ™ci na stosie
	pop rbp            ; przywrÃ³cenie poprzedniej wartoÅ›ci ebp
	ret                ; powrÃ³t
