global iloczyn
iloczyn: 
	push rbp           ; zachowujemy starÄ… wartoÅ›Ä‡ ebp
	mov rbp, rsp     
	
	mov rax, 1
	mov rbx, rdx	

	cmp rdi, 0
	je end
	mul rsi
	dec rdi

	cmp rdi, 0
	je end
	mul rbx
	dec rdi
		
	cmp rdi, 0
	je end
	mul rcx
	dec rdi
	
	cmp rdi, 0
	je end
	mul r8
	dec rdi
	
	cmp rdi, 0
	je end
	mul r9
	dec rdi
	
	
	 
	mul_stack_val:	
		cmp rdi, 0
		je end_mul_stack_val 			
		mov rcx, [rbp + 8 + 8 * rdi]
		mul rcx

		dec rdi
		jmp mul_stack_val		
		
	end_mul_stack_val:
	
	end:	
	mov rsp, rbp       ; zwolnienie pamiÄ™ci na stosie
	pop rbp            ; przywrÃ³cenie poprzedniej wartoÅ›ci ebp
	ret
