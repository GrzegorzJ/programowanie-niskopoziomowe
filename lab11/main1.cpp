#include <stdio.h>

#define N 10

void kopiuj(int * cel, int * zrodlo, unsigned int n) {
		//In the constraint, 'a' refers to EAX, 'b' to EBX, 'c' to ECX, 'd' to EDX, 'S' to ESI, and 'D' to EDI
		//btw czasmi brak wciecia rozwala wszystko:)
		asm("cld; rep movsd"
		: // np. =r" (cel) do rejstru wysciowego %0 zostanie przypisany 'cel' dla tego case nie potrzebujemy
		: "S" (zrodlo), "D" (cel), "c" (n)  // wczytaj zrodlo-> edi, cel -> eso, n -> ecx
		: "%eax" //clobbered register -> czyli mogl sie zmienic podczas wykonywania funkcji, infromacja dla compilatora
		);	

/*
		//powinno tez dzialac a nie dziala, cos jest nie tak z rejtrami po wyjsciu :)
		asm("cld; rep movsd, movl %%eax, %0"
		: "=r" (cel) //z wyjsciem nie dziala
		: "S" (zrodlo), "D" (cel), "c" (n) 
		: "%eax" //nie czyscimy rejstrow wejsciowych esi, edi, ecx wteyd bledy z eax
		);
*/
		//inna wersja
		//http://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html#s5
	
} 

int main(void)
{
    
	int cel[N+1], zrodlo[N], n=N;

	printf("Zrodlo \n");	
	for(int i = 0; i < N; i++) {
		zrodlo[i] = i;
		printf("%d ", zrodlo[i]);
	}
	
	kopiuj(cel, zrodlo, n);	

	printf("\nCel(po skopiowaniu) \n");	
	for(int i = 0; i < N; i++) {
		printf("%d ", cel[i]);
	}
   printf("\n");
    
}



; Rejestry ogólnego przeznaczenia
"a" = eax, ax, al 
"b" = ebx, bx, bl 
"c" = ecx, cx, cl
"d" = edx, dx, dl
"S" = esi, si
"D" = edi, di 
"A" = para rejestrów EDX:EAX 
      (użyteczne np. przy zwracaniu 64 bitowych wyników)
"r" = dowolny rejestr ogólnego przeznaczenia
"q" = jeden z rejestrów a, b, c, d
 
; Adres pamięci  
"m" = zmienna w pamięci, 
      operacje są wykonywane bezpośrednio na pamięci, 
      powinno być używane gdy nie chcemy przechowywać 
      wartości w rejestrze 
 
; Rejestry zmiennoprzecinkowe
"f" = dowolny rejestr zmiennoprzecinkowy
"t" = ST0
"u" = ST1
 
; Stałe
"i" = stała całkowita,
"I" = stała z zakresu 0..31 (dla przesunięć)
"N" = stała z zakresu 0..255 
"E" = stała zmiennoprzecinkowa
 

asm("movl %%ecx, %%eax ;" 
    "addl %%ebx %%eax;");

asm ( "szablon instrukcji asemblerowych" 
      : wyjściowe operandy               /* optional */
      : wejściowe operandy               /* optional */
      : lista niszczonych obiektów       /* optional */
     );
