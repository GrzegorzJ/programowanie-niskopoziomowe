;char * dodaj(char * tab1, char * tab2, int n);
global dodaj


; rdi tab1
; rsi tab2
; rdx n

dodaj:

mov r9, rdx ; przechowuje cala sume
and rdx, -15 ; zamienia na najlbizsza wielokrotnosc 16 np 36 100100 and (1)1111 100000 -> 32 (cos nie dokonca dziala dla nieprzystych :))

mov rcx, 0


dodaj_loop:
	movups xmm0, [rdi + rcx]
	movups xmm1, [rsi + rcx]
	paddsb xmm0, xmm1	
	movups [rax+rcx], xmm0 ; wynik zwracany w rejstrze rax (moze warto jakos zalokowac :) ale i tak dziala)
	
	add rcx, 16 ; dodalismmy na raz 16 bajtow bo tyle pobral movups
	cmp rcx, rdx
jl dodaj_loop 


;dodajemy bajt po bajcie pozostala reszte
dodawanie_koncowki:
	cmp rcx, r9
	je koniec
	
	mov bl, [rsi+rcx]
	mov r8b, [rdi+rcx]
	add r8b, bl
	mov  [rax+rcx], r8b
	
	inc rcx
	jmp dodawanie_koncowki
	
koniec:
	ret

