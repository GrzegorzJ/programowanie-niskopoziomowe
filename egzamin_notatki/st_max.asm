;int run(int level, int * max, ...); 
;ebx, ecx, edx, esi, edi, ebp
BITS 32             ;( 1)
section .text       ;( 2)
global _run, run    ;( 3)
_run:               ;( 4)
run:                ;( 5)
  enter 8, 0        ;( 6) //rezerwujemy 8 bitów, to samo co push ebp mov ebp, esp sub esp, 5
  mov [esp], ebx    ;( 7)
  mov edi, 0        ;( 8)
  sub edx, edx      ;( 9)
  mov ecx, [ebp+8]  ;(10) ;level
  lea esi, [ebp+16] ;(11) ; ...
  cld               ;(12) clears direction flag
.loop:              ;(13) ;run(5, &max,2,6,4,-2,10,5,3,0)
  lodsd             ;(14)Load doubleword at address DS:(E)SI into EAX; stosd mov edx, eax movd mov edx, esi
						;2,6,4,-2,10,5,3,0
  cmp eax, 0        ;(15)
  je .end           ;(16)
  cmp ecx, eax      ;(17);level(5)    
  cmovg eax, edi    ;(18)
  cmovle ebx, eax   ;(19) b+=2
  add edx, eax      ;(20)d+=0
  jmp .loop         ;(21)
.end:               ;(22)
  mov esi, [ebp+12] ;(23)
  mov eax, edx      ;(24)
  mov [esi],ebx     ;(25)
  pop ebx           ;(26);brak push
  leave             ;(27)
  ret               ;(28)
                    ;(29)

;http://www.cs.virginia.edu/~evans/cs216/guides/x86.html
