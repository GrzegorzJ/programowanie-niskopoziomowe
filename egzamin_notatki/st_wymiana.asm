;RDI, RSI, RDX, RCX, R8, R9
BITS 64                                                        ;( 1)
                                                               ;( 2)
section .text                                                  ;( 3)
global wypelnij, _wypelnij, wymiana, _wymiana, szukaj, _szukaj ;( 4)
;  char z = 'A';                                   
;  char tab[]="12345678901234567890";              
;  wypelnij(z, tab);     

wypelnij:                                                      ;( 5)
_wypelnij:                                                     ;( 6)
    push rcx                                                   ;( 7)
    xor rax, rax                                               ;( 8)//dil -> lower 8 bits rdi
    mov al, dil                                                ;( 9)//rax zawiera litery do wypelnienie np 'A'
    mov rdx, 3                                                 ;(10)
    mov cl, 8                                                  ;(11)
.loop1:                                                        ;(12)
    mov r9, rax                                                ;(13)r9->65|r9->16705|1094795585
    shl rax, cl                                                ;(14)65<<8(*2^8)=16640|(cl->16)1094778880|
    add rax, r9                                                ;(15)16705|1094795585|4702111233380188160
    shl cl, 1                                                  ;(16)16|32|64
    sub rdx, 1                                                 ;(17)2|1|0
    jnz .loop1                                                 ;(18)ok|ok|no
    mov rcx, 2                                                 ;(19)
.loop2:                                                        ;(20)2
    dec rcx                                                    ;(21)
    mov [rsi + rcx * 8], rax                                   ;(22)1
    jnz .loop2                                                 ;(23) 0x4141414141414141  4702111234474983745| 0x41->65->'A'
    pop rcx                                                    ;(24)
    ret                                                        ;(25)
                                                               ;(26)
;char tab[]="12345678901234567890";
;printf("%d %d\n", szukaj(tab), szukaj(tab+9));
;16 0
szukaj:                                                        ;(27)
_szukaj:                                                       ;(28)
    push rdi                                                   ;(29)
    xor  rax, rax                                              ;(30)
.search:                                                       ;(31)
    scasb                                                      ;(32)SCASB compares the byte in AL with the byte at [ES:DI] or [ES:EDI], and sets 																	   the flags accordingly. It then increments or decrements (depending on the 																direction flag: increments if the flag is clear, decrements if it is set) DI (or EDI).
    jne  .search                                               ;(33)
    dec  rdi                                                   ;(34)140737488347636
    mov  rax, rdi                                              ;(35)140737488347636
    pop  rdi                                                   ;(36)140737488347625
    sub  rax, rdi                                              ;(37)11
    and  rax, -16                                              ;(38)0 ( and rax,0xfffffffffffffff0 )
    ret                                                        ;(39)
                                                               ;(40)
wymiana:                                                       ;(41)
_wymiana:                                                      ;(42)
    push rbp                                                   ;(43)
    mov  rbp, rsp                                              ;(44)
    shl rcx, 8                                                 ;(45)
    mov cl, dl                                                 ;(46)
    shl rcx, 8                                                 ;(47)
    mov cl, sil                                                ;(48)
    push rcx                                                   ;(49)
    and  rsp, 0xfffffffffffffff0                               ;(50)
    sub  rsp, 64                                               ;(51)
    push rdi                                                   ;(52)
                                                               ;(53)
    mov  rcx, 3                                                ;(54)
    lea  rsi, [rsp+8]                                          ;(55)
.loop1:                                                        ;(56)
    mov  dil, [rbp+rcx-9]                                      ;(57)
    call wypelnij                                              ;(58)
    add  rsi, 16                                               ;(59)
    loop .loop1                                                ;(60)
                                                               ;(61)
    mov  rdi, [rsp]                                            ;(62)
    call szukaj                                                ;(63)
    pop  rdi                                                   ;(64)
    mov  rsi, rdi                                              ;(65)
    add  rdi, rax                                              ;(66)
                                                               ;(67)
.loop2:                                                        ;(68)
    cmp rsi, rdi                                               ;(69)
    jge .koniec                                                ;(70)
    movdqu xmm1, [rsi]                                         ;(71)
    movdqa xmm2, xmm1                                          ;(72)
    movdqu xmm4, [rsp]                                         ;(73)
    movdqu xmm0, [rsp +32]                                     ;(74)
    pcmpgtb xmm0, xmm1                                         ;(75)
    paddb  xmm1, [rsp+16]                                      ;(76)
    pand xmm4, xmm0                                            ;(77)
    pandn xmm0, xmm1                                           ;(78)
    por xmm4, xmm0                                             ;(79)
    movdqu [rsi], xmm4                                         ;(80)
    add rsi, 16                                                ;(81)
    jmp .loop2                                                 ;(82)
.koniec:                                                       ;(83)
    mov rsp,rbp                                                ;(84)
    pop rbp                                                    ;(85)
    ret                                                        ;(86)
