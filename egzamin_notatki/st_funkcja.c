#include <stdio.h>               //( 1)
int fun(int * tab, int rozmiar); //( 2)
int main(){                      //( 3)
  const int rozmiar = 3;         //( 4)
  int tab[] = {1, 2, 3, 4, 5};   //( 5)
  int i = 0;                     //( 6)
  int wynik = fun(tab, rozmiar); //( 7)
  for(i=0; i<rozmiar; i++)       //( 8)
    printf("%d ", tab[i]);       //( 9)
  printf("\n%d\n", wynik);       //(10)
}                                //(11)

//4 2 16 
//2

