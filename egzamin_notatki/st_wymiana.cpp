#include <cstdio>                                 //( 1)
using namespace std;                              //( 2)
extern "C" void wymiana(char*, char, char, char); //( 3)
extern "C" void wypelnij(char, char*);            //( 4)
extern "C" int szukaj(char*);                     //( 5)
                                                  //( 6)
int main() {                                      //( 7)
  char z = 'A';                                   //( 8)
  char tab[]="12345678901234567890";              //( 9)
  wypelnij(z, tab);                               //(10)
  printf("[%s]\n", tab);                          //(11)
  printf("%d %d\n", szukaj(tab), szukaj(tab+9));  //(12)
  char txt[]="To jest tekst probny";              //(13)
  wymiana(txt, 'k','A'-'a','*');                  //(14)
  //wymiana(txt, 'f',1,'#');                      //(15)
  printf("[%s]\n", txt);                          //(16)
  return 0;                                       //(17)
}                                                 //(18)


//[AAAAAAAAAAAAAAAA7890]
//16 0
//[*O***ST*T*KST*PRobny]
