 #include <stdio.h>                       //( 1)
int run(int level, int * max, ...);       //( 2)
int main(){                               //( 3)
  register int a = 1;                     //( 4)
  int max =0.0, wynik=0.0;                //( 5)
  wynik = run(5, &max,2,6,4,-2,10,5,3,0); //( 6) 2 + 4 -2 + 5 + 3
  printf("%d %d\n", wynik, max);          //( 7)
  wynik = run(3, &max,2,6,4,-2,10,0);     //( 8)
  printf("%d %d\n", wynik, max);           //( 9)
  printf("%d\n", a);                      //(10)
}                                         //(11)

//Output:
//21 5
//20 10
//1

