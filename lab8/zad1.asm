;unsigned int rotate(unsigned int x, int n = 1);

;unsigned char rotate(unsigned char x, int n = 1);
BITS 32
section .text 

global _Z6rotateji        ;KONWENCJA!!! - funkcja suma ma być widziana w innych modułach aplikacji
                   ; pod Windowsem należy dodać podkreślenie przed suma
 
_Z6rotateji:    

%define x dword [ebp+8]
%define n dword [ebp+12]

enter 0,0

mov eax, x
mov ecx, n
rol eax, cl

leave                           
ret

global _Z6rotatehi        ;KONWENCJA!!! - funkcja suma ma być widziana w innych modułach aplikacji
                   ; pod Windowsem należy dodać podkreślenie przed suma
 
_Z6rotatehi:    

%define x dword [ebp+8]
%define n dword [ebp+12]

enter 0,0

mov eax, x
mov ecx, n
rol eax, cl

leave                           
ret

