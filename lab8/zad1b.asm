;unsigned int rotate(unsigned int x, int n = 1);

;unsigned char rotate(unsigned char x, int n = 1);


%define x dword [ebp+8]
%define n dword [ebp+9]

enter 0,0

segment .text

global rotate
 rotate:

mov eax,[x]
mov cl, [n]
rol eax, cl

leave                           
ret
 
