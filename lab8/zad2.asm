
BITS 32
section .text 

global _Z6kopiujPiS_j
 
_Z6kopiujPiS_j:    

%define cel dword [ebp+8]
%define zrodlo dword [ebp+12]
%define n dword [ebp+16]

enter 0,0

cld
mov esi, zrodlo
mov edi, cel
mov ecx, n
 
rep movsd

leave                           
ret

global _Z5zerujPij
 
_Z5zerujPij:    

%define tablica dword [ebp+8]
%define n dword [ebp+12]

enter 0,0

cld
mov eax, 0
mov edi, tablica
mov ecx, n
 
rep stosd

leave                           
ret

