// KOMPILACJA - kod źródłowy C w main.c, kod źródłowy ASM w suma.asm
// LINUX :
// jak sa problemy z kompilacja c na linuxie to:
// zainstalowac http://stackoverflow.com/questions/23498237/compile-program-for-32bit-on-64bit-linux-os-causes-fatal-error
// nasm -felf32 suma.asm -o suma.o
// gcc -m32 -o main.o -c main.c
// gcc -m32 main.o suma.o -o suma
// 
// WINDOWS :
// nasm -fcoff suma.asm -o suma.obj
// gcc -o main.obj -c main.c
// gcc main.obj suma.obj -o suma.exe
#include <iostream>

unsigned int rotate(unsigned int x, int n = 1);
unsigned char rotate(unsigned char x, int n = 1);

int main()
{
	unsigned int i = 10;
	unsigned char c = 60;
  	std::cout << rotate(i, 2) << std::endl;
    std::cout << rotate(c, 1) << std::endl;


  return 0;
}
/* Wyjście:
 * 14
 * 2
 */ 