// KOMPILACJA - kod źródłowy C w main.c, kod źródłowy ASM w suma.asm
// LINUX :
// jak sa problemy z kompilacja c na linuxie to:
// zainstalowac http://stackoverflow.com/questions/23498237/compile-program-for-32bit-on-64bit-linux-os-causes-fatal-error
// nasm -felf32 suma.asm -o suma.o
// gcc -m32 -o main.o -c main.c
// gcc -m32 main.o suma.o -o suma
// 
// WINDOWS :
// nasm -fcoff suma.asm -o suma.obj
// gcc -o main.obj -c main.c
// gcc main.obj suma.obj -o suma.exe
#include <iostream>

// kopiuje n liczb typu int z zrodla do celu 
void kopiuj(int * cel, int * zrodlo, unsigned int n);

// zeruje tablice liczb typu int o rozmiarze n
void zeruj(int * tablica, unsigned int n);

int main()
{
	int tab1[5] = {0, 0, 0, 0, 0};
	int tab2[5] = {5, 4, 3, 2, 1};

	kopiuj(tab1, tab2, 3);

  	int n;
  	for ( n=0 ; n<5 ; ++n ){
    	std::cout << tab1[n];
  	}
  	std::cout << std::endl;

  	zeruj(tab2, 4);
    for ( n=0 ; n<5 ; ++n ){
    	std::cout << tab2[n];
  	}
  	std::cout << std::endl;


  return 0;
}
/* Wyjście:
 * 14
 * 2
 */ 