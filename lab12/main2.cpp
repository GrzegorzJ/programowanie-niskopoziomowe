#include<stdio.h>

using namespace std;
extern "C" void solve(int n, float * A, float * B, float * X);

int main() {
	const int N = 8;
	float A[N] = {1, 2, 3, 4, 5, 6, 7, 8};
	float B[N] = {2, 3, 4, 5, 6, 7, 8, 9};
	float X[N];
	
	solve(N, A, B, X);
	for(int i = 0; i < N; i++){
		printf(" %d: \n", X[i]);
	}
}
