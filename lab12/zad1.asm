global minmax

;void minmax(int n, int * tab, int * max, int * min);
; rdi n
; rsi tab
; rdx max
; rcx min
minmax:
	push rbx
	enter 0, 16
	push rdx
	push rcx
	

	mov rdx, 0
	mov rcx, 0
	mov rax, 0

	mov dl, [rsi]
	mov cl, [rsi]

	mov rbx, 0 ;counter

minmax_loop:
	cmp rbx, rdi ;rbx - counter, rdi -> n 
	je end_minmax_loop	

	mov eax, [rsi + 4 * rbx] 

	;max
	cmp rax, rdx
	cmovg rdx, rax
	
	;min
	cmp rax, rcx
	cmovl rcx, rax

	inc rbx
	jmp minmax_loop

end_minmax_loop:
	pop rax
	mov [rax], ecx 
	pop rax
	mov [rax], edx

	leave  
	pop rbx
	ret
	
