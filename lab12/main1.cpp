#include<time.h>
#include<stdio.h>

using namespace std;
extern "C" void minmax(int n, int * tab, int * max, int * min);

void minmaxcpp(int n, int * tab, int * max, int * min) {
	int i;
	for(i=0; i<n; ++i) {
		if(tab[i] > *max) {
			*max = tab[i];
		}
		if(tab[i] < *min) {
			*min = tab[i];
		}
	}
}

int main()
{
	const int rozmiar = 17;	
	int tab[rozmiar] = {2, 3, 3, 65, 3, 123, 4, 32, 342, 22, 5, 11, 32, 44, 12, 324, 43};
	const int liczba_powtorzen = 10000;

	int min, max;
    clock_t start, stop;
    start = clock();
    for(int i=0; i<liczba_powtorzen; i++){
       minmax(rozmiar, tab, &max, &min);
    }
    printf("min = %d    max = %d\n", min, max); 
    stop = clock();
    printf("time = %f  ( %f cykli)\n", (stop - start)*1.0/CLOCKS_PER_SEC, (stop - start)*1.0);

    min = 1000; max = 0;
    start = clock();
    for(int i=0; i<liczba_powtorzen; i++){
       minmaxcpp(rozmiar, tab, &max, &min);
    }
    printf("min = %d    max = %d\n", min, max); 
    stop = clock();
    printf("time = %f  ( %f cykli)", (stop - start)*1.0/CLOCKS_PER_SEC, (stop - start)*1.0);
	return 0;
}
