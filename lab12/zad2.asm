global solve

section .data
	zero_vector dd 0.0, 0.0, 0.0, 0.0
	once_vector dd 1.0, 1.0, 1.0, 1.0
	inf_vector dd 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF
	NaN_vector dd 0xABCD, 0xABCD, 0xABCD, 0xABCD
solve:
	enter 0, 0
; rdi n
; rsi A
; rdx B
; rcx X

mov rax, 0 ; counter

solve_loop:
	cmp rdi, rax
	je solve_loop_end 
	
	movups xmm0, [rsi + rax]; A
	movups xmm1, [rdx + rax]; B
	movups xmm2, [zero_vector]
	movups xmm4, xmm0; X
	;xmm0 -> A
	;xmm1 -> B
	;xmm2 -> zero vector

	
	;dzielimy wszystko a nastepnie  bedziemy podmieniac wartosci dla a==b a==0&b!=0 
	divps xmm4, xmm1 ; x=b/a
	
	ucomiss xmm0, xmm2 ; AZeroMask
	ucomiss xmm1, xmm2 ; BZeroMask

	andnps xmm0, xmm1 ; xmm0 0 tam gdzie a==b==0
	mulps xmm4, xmm0 ; zerujemy miejsca gdzie a==b==0

	xorps xmm0, [once_vector] ; zamieniamy kolejnosc
	mulps xmm0, [inf_vector]; inf if a==b==0 else 0 
	
	addps xmm4, xmm0 ; dodajemy inf do	a==b==0
	
	;towrzmy maske dla a==0 i b!=0 t
	ucomiss xmm0, xmm2 ; isAZeroMask
	xorps xmm1, [once_vector] ;negation BZeroMask 
	andps xmm0, xmm1 ;a==0 i b!=0
	
	mulps xmm0, [NaN_vector]
	addps xmm4, xmm0
			
	add rax, 4	
	jmp solve_loop

solve_loop_end:
	xor rax, rax
	leave 	
	ret



