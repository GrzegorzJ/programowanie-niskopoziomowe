;Grzegorz Jaromin
;nasm -f elf mult.asm -o mult.o
;gcc -m32 -o mult mult.o

section .text
extern printf

global main

main:		
	;create the stack pointer
	push ebp ;save the callers frame pointer
	mov	ebp, esp ;  create the new frame pointer

	mov esi, 1 ; number to check	
	mov ecx, esi 
	dec ecx	; start checkinf from n-1

	.checking_loop:      
		cmp ecx, 2   
		jbe .number_is_prime ;the divisor was not found so the number is prime (i<=0)
		dec ecx	;decrement counter	
		mov eax, esi
		mov edx, 0
		div ecx
		cmp edx, 0 ;if reminder is not equal 0 then continue checking
		je .number_is_not_prime
		jmp .checking_loop

	.number_is_not_prime:
		push esi
		push esi
		push is_not_prime_msg
		call printf
		add esp, 8
		pop esi	
		jmp .end

	.number_is_prime:
		push esi		
		push esi
		push is_prime_msg
		call printf
		add esp, 8
		pop esi				
		jmp .end	
								
	.end:
		;destroy stack frame
		mov esp, ebp ;restore stack pointer
		pop ebp ;restore callers stack pointer
		ret ;return to caller	
	
section .data
	is_prime_msg: db "The %d is prime", 10, 0
	is_not_prime_msg: db "The %d is not prime", 10, 0 
