;Grzegorz Jaromin
;nasm -f elf mult.asm -o mult.o
;gcc -m32 -o mult mult.o

section .text
extern printf

global main

main:		
	;create the stack pointer
	push ebp ;save the callers frame pointer
	mov ebp, esp ;  create the new frame pointer

	mov esi, 100 ; number for wich divisors will be printed

	mov ecx, esi ; loop inedx equal number to check
	
	jmp .checking_loop 	

	.checking_loop:      
		cmp ecx, 0  		
		jbe .end ; the divisor was not found so the number is prime (i<=0)
		dec ecx		
		mov eax, esi
		mov edx, 0
		div ecx
		cmp edx, 0 ;if reminder is equal 0 print divisor
		je .print_divisor
		jmp .checking_loop

	.print_divisor:
		push ecx ; store ecx
		push ecx ;loop counter contains currently checking divisor		
		push divisor_msg
		call printf	
		add esp, 8
		pop ecx	  	
		jmp .checking_loop
								
	.end:
		;destroy stack frame
		mov esp, ebp ;restore stack pointer
		pop ebp ;restore callers stack pointer
		ret ;return to caller	
	
section .data
	divisor_msg: db "The %d is the divisor", 10, 0 
