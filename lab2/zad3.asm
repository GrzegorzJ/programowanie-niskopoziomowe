;Grzegorz Jaromin
;nasm -f elf zad3.asm -o zad3.o
;gcc -m32 -o zad3 zad3.o

section .text
extern printf

global main

main:		
	;create the stack pointer
	push ebp ;save the callers frame pointer
	mov ebp, esp ;  create the new frame pointer

	;Accessible 32-bit register (EAX, EBX, ECX, EDX, ESI, EDI, ESP, or EBP)	

	mov esi, 90  ; number 1
	mov edi, 60 ; number 2
   ;number 1 should be greater then 1
	
	mov ecx, edi ; starting checking from 2
	mov ebx, 1; stores greates common divisor

	.find_common_divisor: 	
		cmp ecx, 1
		jbe .print_greates_common_divisor ; all divisors from 2-num2(the smallest one) have been checked	
		jmp .divide_by_counter ; counter contains current num to check if it is dividor

	.divide_by_counter:
		; checking first number		
		mov eax, esi
		mov edx, 0
		div ecx
		cmp edx, 0 ; if the first number is divided then the second one is checked
		je .check_second_number
		dec ecx
		jmp .find_common_divisor ; if the first number is not divided by the counter then it is incremented and checked again 

	.check_second_number:	
		mov eax, edi
		mov edx, 0
		div ecx
		cmp edx, 0		
		je .update_commom_divisor
		dec ecx
		jmp .find_common_divisor

	.update_commom_divisor:
		mov ebx, ecx ; current counter is now the gcd
		inc ecx ; check if higer 
		jmp .print_greates_common_divisor ; starting from the higest so there is no point to check more

	.print_greates_common_divisor:
		push ebx ; store gcd
		push ebx ;		
		push gcd_msg
		call printf	
		add esp, 8
		pop ebx	  	
		jmp .end
	
	.end:
		;destroy stack frame
		mov esp, ebp ;restore stack pointer
		pop ebp ;restore callers stack pointer
		ret ;return to caller	
	
section .data
	gcd_msg: db "The greates common divisor is %d ", 10, 0 
	
