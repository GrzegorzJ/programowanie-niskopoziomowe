#!/bin/bash
file_name=$1
mode=$2 #either 32 or 64

function compile_and_run_64(){
	#clear
	echo "Compile, link and run in 64 mode: " $file_name
	echo "#####################"
	nasm -felf64 $file_name.asm -o $file_name.o
	ld $file_name.o -o $file_name #if printf is used it has to be chaned to gcc
	./$file_name
}


function compile_and_run_32(){
	#clear
	echo "Compile, link and run in 32 mode : "${file_name}
	echo "#####################"
	nasm -felf32 $file_name.asm -o $file_name.o
	#ld -m elf_i386 $file_name.o -o $file_name 
	gcc -m32 -o $file_name $file_name.o #needed when printf is used	
	./$file_name
}  


if [ -z "$mode" ] || [ $mode -eq "64" ]; then #if mode is not set the default 64 mode is activated 
	mode=64
	compile_and_run_64
elif [ $mode -eq "32" ]; then
	compile_and_run_32
else
	echo "Incorrect mode " $mode
fi


#sample run command
#./compile_and_run.sh zad1    //for 64 bit mode
#./compile_and_run.sh zad1 64 //for 64 bit mode
#./compile_and_run.sh zad1 32 //for 32 bit mode
