;void diff(char *out,char *bufor1,char *bufor2,int n);
;out[i] = bufor1[i] - bufor2[i]
global diff


; rdi out
; rsi bufor1
; rdx bufor2
; rcx n

;general registers
;RDI, RSI, RDX, RCX, R8, R9
diff:

mov rax, rcx
mov r9, rcx ; przechowuje cala sume

and rax, -15 ; zaokreglenie do pelnej wilokrotnosc 16
and rax, 0xe ; dla nieparystych powyzsza operacja np dla 37 daje 33 zamiast 32

mov rcx, 0

cmp r9, 16
jl odejmowanie_koncowki

odejmowanie:
	movups xmm0, [rsi + rcx]
	movups xmm1, [rdx + rcx]
	psubq xmm0, xmm1 ; dzieli rejestr xmm0 na 2 czesci i rownolegle odejmuje kazda z nich osobno		
	movups [rdi+rcx], xmm0

	add rcx, 16
	cmp rcx, rax
	jl odejmowanie 


odejmowanie_koncowki:
	cmp rcx, r9
	je koniec
	
	mov r8b, [rsi+rcx]
	mov r12b, [rdx+rcx]
    sub r8b, r12b	
;
	mov [rdi+rcx], r8b
	
	inc rcx
	jmp odejmowanie_koncowki

koniec:
	xor rax, rax
	ret


;PD (packed double) – działanie na wektorach podwójnej precyzji,
;PS (packed single) – działanie na wektorach pojedynczej precyzji,
;SD (scalar double) – działanie na skalarach podwójnej precyzji.
;SS (scalar single) – działanie na skalarach pojedynczej precy


;dodawanie (ADDPD, ADDPS, ADDSD, ADDSS)
;odejmowanie (SUBPS, SUBSS)
;mnożenie (MULPS, MULSS)
;dzielenie (DIVPS, DIVSS)
;przybliżenie odwrotności (1/x) (RCPPS, RCPSS)
;pierwiastek kwadratowy (SQRTPS, SQRTSS)
;przybliżenie odwrotności pierwiastka kwadratowego (RSQRTPS, RSQRTSS)
;wyznaczenie minimalnej wartości (MINPS, MINSS)
;wyznaczenie maksymalnej wartości (MAXPS, MAXSS)

;g++ -m64 -o main$1.o -c main$1.cpp
;nasm -felf64 -o zad$1.o zad$1.asm
;g++ -m64 main$1.o zad$1.o -o zad$1

