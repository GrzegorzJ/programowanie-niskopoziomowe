#include <stdio.h>

#define N 41

// na wyjściu out[i] = bufor1[i] - bufor2[i]
extern "C" void diff(char *out,char *bufor1,char *bufor2,int n);

int main(void)
{
    char Tablica[N+1],DIFF[N];
    int i;
	
	//DIFF[i]=T[i+1]-T[i]
    Tablica[0]=1; //1 2 4 7 11 16 22 
	//T[i+1]-T[i] = 1 2 3 4 5
    for(i=1;i<=N;i++) Tablica[i]=Tablica[i-1]+i;

    diff(DIFF, Tablica+1, Tablica, N);

    for(i=0;i<N;i++) printf("%d ",DIFF[i]);
    printf("\n");
    
}

