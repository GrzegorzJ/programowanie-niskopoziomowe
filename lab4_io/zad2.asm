%include "asm64_io.inc"

section .data

	array times 6 db 0

section .bss


section .text

global asm_main
asm_main:
enter 0,0 


mov eax, 0
mov ecx, 0
call read_int

;loop with division
div_loop:
	cmp eax, 0
	je end_div_loop

	mov edx, 0
	mov edi, 10
	div edi 				; EAX = (EDX:EAX div EDI),
              				; EDX = (EDX:EAX mod EDI)
    push rdx 				;put numbers on stack
    inc ecx
    jmp div_loop
end_div_loop:

print_loop:					;loop and pop numbers from stack
	cmp ecx, 0
	je end_print_loop

	pop rax 
	call print_int
	call print_nl

	dec ecx
	jmp print_loop
end_print_loop:


mov rax, 0 
leave
ret

