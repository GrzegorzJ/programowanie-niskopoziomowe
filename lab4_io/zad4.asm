%include "asm64_io.inc"

section .data

	array times 20 db 0
	hex_map db 48,49,50,51,52,53,54,55,56,57,65,66,67,68,69,70, 0ah
	;1,2,3,4,5,6,7,8,9,A,B,C,D,E,F
	number dd 40000 ;4 bit int
	text db "       ",0
section .bss


section .text

global asm_main
	asm_main:

enter 0,0
; ---- start
;prepare stack
mov rax, 0
mov rax, ret_convert			;save return address
push rax						;stack: ret address

mov rax, 0
mov eax, [number]
push rax						;stack: ret address, number

jmp short convert_hex
ret_convert:

mov rax, rsp					;stack: number, hex_string
call print_string
pop rbx							;stack: number
pop rbx							;stack:

call print_nl
; --- end
mov rax, 0 
leave
ret

convert_hex:
	pop rax						;number
								;stack: ret address
	pop rsi						;back jump address
								;stack:
	push rax						;stack: number

	mov ebx, 6
	petla_while: 
		cmp	    eax, 0
		jle	    koniec_petli

		mov edx, 0			;clear before div
		mov edi, 16
		div edi 				; EAX = (EDX:EAX div EDI),
	              				; EDX = (EDX:EAX mod EDI)

	    mov ecx, [hex_map + edx]
	    mov [text + ebx], cl

		dec ebx
		jmp	petla_while
	koniec_petli:

	; mov rax, text
	; call print_string

	mov rax, [text]
	push rax					;stack: number, hex_string
jmp rsi