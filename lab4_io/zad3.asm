%include "asm64_io.inc"

section .data

	arr dd 1,3,2,5,6,4
	arr_size dd 6

section .bss
	size resd 1

section .text

global asm_main
asm_main:
enter 0,0 

;print input arr
mov eax, 0
mov	ecx, 0					
print_loop: 
	cmp	ecx, [arr_size]		
	jge end_print_loop
	
	mov eax, [arr + ecx * 4]
	call print_int
		
	inc	ecx		        	
	jmp	print_loop
end_print_loop:


;prepare stack
mov rax, 0
mov eax, arr 				;push array pointer
push rax
mov eax, [arr_size]			;push array size
push rax

;sort arr
mov rsi, 0
mov esi, ret_sort			;save return address
jmp short sort
ret_sort:


call print_nl
;print sorted arr
mov eax, 0
mov	ecx, 0					
print_sorted_loop: 
	cmp	ecx, [arr_size]		
	jge end_print_sorted_loop
	
	mov eax, [arr + ecx * 4]
	call print_int
		
	inc	ecx		        	
	jmp	print_sorted_loop
end_print_sorted_loop:

call print_nl
mov eax, edx

mov rax, 0 
leave
ret
;--end


;sort function
sort:
	pop rax							;array size
	mov [size], eax
	pop rax							;array pointer

	mov ecx, 0
	sort_loop:
		cmp ecx, [size]
		jge end_sort_loop

		;--find min 
		mov ebx, 999999				;MIN
		mov edx, 0					;INDEX
		mov edi, ecx
		min_loop:
			cmp edi, [size]
			jge end_min_loop
			
			cmp [eax + edi * 4], ebx
			jge move_next
			mov ebx, [eax + edi * 4]
			mov edx, edi

			move_next:
			inc edi
			jmp min_loop
		end_min_loop:

		;--
		mov edi, [eax + ecx * 4]	;edi is tmp var
		mov [eax + ecx * 4], ebx	;move min number into right place
		mov [eax + edx * 4], edi 	;restore tmp value

		inc ecx 
		jmp sort_loop
	end_sort_loop:
jmp rsi

