%include "asm64_io.inc"

section .data
;
;

section .bss
	input1 resd 1			;32 bit number
    input2 resd 1

section .text
global asm_main
asm_main:
enter 0,0 
;--start

mov ebx, input1             ; ładujemy adres input1 do ebx
mov ecx, ret1               ; łądujemy adres etykiety ret1 do ecx - do tej etykiety nastąpi powrót z procedury
jmp short get_int           ; wywołanie procedury get_int

ret1:

mov ebx, input2
mov ecx, $ + 7               ; ecx = adres bieżący + 7 
                             ; jest to adres pierwszego bajtu instrukcji do której wracamy z procedury get_int
jmp short get_int

mov eax, [input1]
add eax, [input2]
call print_int

call print_nl
mov rax, 0 ; kod zwracany z funkcji
leave
ret

; subprogram get_int
; Parametry:
; ebx - adres podwójnego słowa,w którym przechowywana będzie wczytana liczba
; ecx - adres instrukcji powrotu
get_int:
call read_int
mov [ebx], eax                  ; zapisujemy dane wejściowe do pamięci
jmp rcx                         ; powrót do procedury wołającej

;--end


