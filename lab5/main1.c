//Napisać w assemblerze funkcję wyliczającą sumę liczb od 1 do n 
//(zadane przez użytkownika). Aplikacja ma być złożona z dwóch modułów w C i ASM.

#include<stdio.h>

int suma(int number);

int main() {
	int numberOfElem = 3;
	printf("Enter number of elements to sum: ");
	scanf("%d", &numberOfElem);	
	printf("The sum is : %d \n", suma(numberOfElem));
	return 0;
}
