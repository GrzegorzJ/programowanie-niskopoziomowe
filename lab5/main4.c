/*
Napisać moduł asemblerowy implementujący funkcję minmax  wyliczającą minimalny
i maksymalny spośród argumentów funkcji. Pierwszym argumentem funkcji jest liczba 
całkowita N>0, po której następuje N argumentów całkowitych.   
Wyniki mają być zwracane jako struktura MM
*/
#include<stdio.h>

typedef struct{
    int min;
    int max;
} MM;

MM minmax( int N, ...);

int main(){
   MM wynik = minmax(5, 1, -2, 4 , 90, 4);
   printf("min = %d, max = %d \n", wynik.min, wynik.max);
   return 0;
}
