BITS 32
section .text 

global minmax        ;KONWENCJA!!! - funkcja suma ma być widziana w innych modułach aplikacji
                   ; pod Windowsem należy dodać podkreślenie przed suma
 
minmax:
push   ebp                    ;KONWENCJA!!! - tworzymy ramkę stosu na początku funkcji
mov    ebp, esp            

; po wykonaniu push ebp i mov ebp, esp:
; w [ebp]    znajduje się stary EBP
; w [ebp+4]  znajduje się adres powrotny z procedury
; w [ebp+8]  wskaznik na sytukture MN
; w [ebp+12] ilosc elementow N
; w [ebp+12 + 4 *i] ilosc elementow N, i <= N
; itd.

%idefine    numberOfElem    [ebp+12]
;ebx ma byc nie zmieniony
push ebx

mov eax, [ebp + 12 + 4] ; min
mov ebx, [ebp + 12 + 4] ; max
mov ecx, numberOfElem


min_max_loop:
	cmp ecx, 0
	je end_min_max_loop

	dec ecx	
	mov edx, [ebp + 12 + 4 * ecx];sprawdzana wartosc 
	
	min_check:
		cmp edx, eax 
		jge max_check
		mov eax, edx	
		jmp	min_max_loop	
		
	max_check:
		cmp edx, ebx
		jle min_max_loop
		mov ebx, edx
	
	jmp min_max_loop	
end_min_max_loop:

mov edx, [ebp + 8]
mov [edx], eax
mov [edx + 4], ebx	
; tu kończy się właściwy kod funkcji

pop ebx

leave                            ; LEAVE = mov esp, ebp / pop ebp - konwencjonalne zakończenie procedury
ret
