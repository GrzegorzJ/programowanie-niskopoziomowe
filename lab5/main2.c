/*
Napisać aplikację wyliczającą sumę elementów wektora danych. Aplikacja ma być złożona z dwóch modułów:
-> w C (inicjalizacja wektora, operacje IO):  
-> w ASM (wyliczenie sumy) argumentami dla funkcji jest ilość elementów tablicy i wskaźnik na pierwszy element tablicy.
*/

#include<stdio.h>

int suma(int *vector, int vector_length);

int main() {
	int vector_length = 4;
	int vector[] = {1, 2, 3, 4};

	printf("The sum of the vector is %d \n", suma(vector, vector_length));

	return 0;
}

