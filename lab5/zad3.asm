BITS 32
section .text 

global sortuj        ;KONWENCJA!!! - funkcja suma ma być widziana w innych modułach aplikacji
                   ; pod Windowsem należy dodać podkreślenie przed suma
 
sortuj:
push   ebp                    ;KONWENCJA!!! - tworzymy ramkę stosu na początku funkcji
mov    ebp, esp            

; po wykonaniu push ebp i mov ebp, esp:
; w [ebp]    znajduje się stary EBP
; w [ebp+4]  znajduje się adres powrotny z procedury
; w [ebp+8]  znajduje się pierwszy parametr - a
; w [ebp+12]  znajduje się pierwszy parametr - b
; w [ebp+16]  znajduje się pierwszy parametr - c
; itd.

%idefine    a    [ebp+8]
%idefine    b    [ebp+12]
%idefine    c    [ebp+16]


;sort(a, b, c)
;	a > b
;	swap(a,b)
;
;	b > c
;	swap(b, c)
;
;	a > b
;	swap(a,b)

; tu zaczyna się właściwy kod funkcji

push edi

mov eax, a
mov ebx, b
mov ecx, c
mov edx, 0 ; tmp


mov edx, [eax]
mov edi, [ebx]

;cmp a,b
cmp edx, [ebx]
	jl no_swap1
		mov [eax], edi
		mov [ebx], edx 
	
no_swap1:

mov edx, [ebx]
mov edi, [ecx]

;cmp c, b
cmp edx, [ecx]
	jl no_swap2
		mov [ecx], edx
		mov [ebx], edi

no_swap2:	

mov edx, [eax]
mov edi, [ebx]

;cmp a,bv
cmp edx, [ebx]
	jl no_swap3	
		mov [eax], edi
		mov [ebx], edx

no_swap3:
mov eax, 0	
; tu kończy się właściwy kod funkcji

pop edi
leave                            ; LEAVE = mov esp, ebp / pop ebp - konwencjonalne zakończenie procedury
ret
