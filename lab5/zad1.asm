BITS 32
section .text 

global suma        ;KONWENCJA!!! - funkcja suma ma być widziana w innych modułach aplikacji
                   ; pod Windowsem należy dodać podkreślenie przed suma
 
suma:
push   ebp                    ;KONWENCJA!!! - tworzymy ramkę stosu na początku funkcji
mov    ebp, esp            

; po wykonaniu push ebp i mov ebp, esp:
; w [ebp]    znajduje się stary EBP
; w [ebp+4]  znajduje się adres powrotny z procedury
; w [ebp+8]  znajduje się pierwszy parametr - liczba elementow do wczytania
; itd.

%idefine    number    [ebp+8]


; tu zaczyna się właściwy kod funkcji

; ((1+n)n)/2
mov eax, number
inc eax
mov ebx, number
mul ebx
mov ebx, 2
div ebx

	
; tu kończy się właściwy kod funkcji

leave                            ; LEAVE = mov esp, ebp / pop ebp - konwencjonalne zakończenie procedury
ret
