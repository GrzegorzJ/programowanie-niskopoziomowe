/*
	Napisac funkcję sortującą rosnąco wartości trzech podanych zmiennych.
	Po wywołaniu funkcji wartości zmiennych powinny zostać odpowiednio pozamieniane. 
*/
#include<stdio.h>

void sortuj( int * a, int *b, int * c);

int main() {
	int a = 3;
	int b = 2;
	int c = 1;
	
	sortuj(&a, &b, &c);
	printf("Sorted number %d, %d, %d \n", a, b, c);

	return 0;
}


