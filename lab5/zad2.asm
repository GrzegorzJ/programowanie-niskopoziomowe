BITS 32
section .text 

global suma        ;KONWENCJA!!! - funkcja suma ma być widziana w innych modułach aplikacji
                   ; pod Windowsem należy dodać podkreślenie przed suma
 
suma:
push   ebp                    ;KONWENCJA!!! - tworzymy ramkę stosu na początku funkcji
mov    ebp, esp            

; po wykonaniu push ebp i mov ebp, esp:
; w [ebp]    znajduje się stary EBP
; w [ebp+4]  znajduje się adres powrotny z procedury
; w [ebp+8]  znajduje się pierwszy parametr - wskaznik na vector
; w [ebp+12] dlugosc vectora
; itd.

%idefine    vector    [ebp+8]
%idefine	vector_length	[ebp+12]


mov eax, 0
mov ebx, vector
mov ecx, vector_length

sum_loop:
	cmp ecx, 0
	je sum_loop_end
	dec ecx
	
	add eax, [ebx + 4 * ecx]		
	jmp sum_loop
sum_loop_end:

	
; tu kończy się właściwy kod funkcji

leave                            ; LEAVE = mov esp, ebp / pop ebp - konwencjonalne zakończenie procedury
ret
