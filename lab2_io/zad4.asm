%include "asm64_io.inc"

section .data
;
	not_prime db 'not a prime number', 0
	prime db 'prime number', 0
;

section .bss
;
; dane niezainicjalizowane
;

section .text
global asm_main
asm_main:
enter 0,0 

; ---- start
mov eax, 0
mov	ecx, 5					; START
petla_for: 
	cmp	    ecx, 20			; STOP
	jge	    koniec_petli	; wychodzimy, gdy i <= 1
	mov eax, ecx
	call print_int
	mov eax, ' '
	call print_char

	call near check
	
	inc	    ecx		        ; 
	jmp	    petla_for
koniec_petli:

; --- end

koniec_programu:
mov rax, 0 ; kod zwracany z funkcji
leave
ret

check:
mov esi, ecx

; ---- start
dec esi 				; start from number - 1
p_for: 
	cmp	    esi, 1	
	jbe	    k_petli	; wychodzimy, gdy i <= 1

	mov edx, 0			;clear before div
	mov eax, ecx	
	mov edi, esi
	div edi 				; EAX = (EDX:EAX div EDI),
              				; EDX = (EDX:EAX mod EDI)
	cmp edx, 0
	jne next

	; not a prime number

	mov eax, not_prime
	call print_string
	call print_nl

	jmp k_programu

	next:
	dec	    esi		        ; i=i-1
	jmp	    p_for
k_petli:
mov     eax, prime
call print_string
call print_nl

; --- end

k_programu:


ret


