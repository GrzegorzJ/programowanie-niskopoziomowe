%include "asm64_io.inc"

section .data
;
;

section .bss
;
; dane niezainicjalizowane
;

section .text
global asm_main
asm_main:
enter 0,0 

; ---- start
mov	ecx, 1		; ECX to zmienna i
mov ebx, 192

petla_for: 
	cmp	    ecx, 1000
	jge	    koniec_petli	; wychodzimy, gdy i >= 1000

	mov edx, 0			;clear before div
	mov eax, ebx			;FIRST_NUMBER - should be lower
	mov esi, ecx
	mul esi 			; EDX:EAX = EAX*ESI

	mov edi, 348			;SECOND_NUMBER
	div edi				; EAX = (EDX:EAX div EDI),
              			; EDX = (EDX:EAX mod EDI)

	cmp edx, 0
	jne next

	;edx == 0
	mov esi, ebx
	mov eax, ecx
	mul esi
	call print_int
	call print_nl
	jmp koniec_programu
	

	next:
	inc	    ecx		        ; i=i-1
	jmp	    petla_for
koniec_petli:

; --- end

koniec_programu:
mov rax, 0 ; kod zwracany z funkcji
leave
ret


