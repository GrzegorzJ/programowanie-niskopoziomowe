%include "asm64_io.inc"

section .data
;
	not_prime db 'not a prime number', 0
	prime db 'prime number', 0
	number db 13
;

section .bss
;
; dane niezainicjalizowane
;

section .text
global asm_main
asm_main:
enter 0,0 

; ---- start
mov eax, 0
mov	ecx, [number]		; ECX to zmienna I. i=number
dec ecx 				; start from number - 1
petla_for: 
	cmp	    ecx, 1	
	jbe	    koniec_petli	; wychodzimy, gdy i <= 1

	mov edx, 0			;clear before div
	mov eax, [number]	;number / ecx
	mov edi, ecx
	div edi 				; EAX = (EDX:EAX div EDI),
              				; EDX = (EDX:EAX mod EDI)
	cmp edx, 0
	jne next

	; not a prime number

	mov     eax, not_prime
	call print_string
	jmp koniec_programu

	next:
	dec	    ecx		        ; i=i-1
	jmp	    petla_for
koniec_petli:
mov     eax, prime
call print_string

; --- end

koniec_programu:
mov rax, 0 ; kod zwracany z funkcji
leave
ret


