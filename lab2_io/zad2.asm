%include "asm64_io.inc"

section .data
;
	number db 11
;

section .bss
;
; dane niezainicjalizowane
;

section .text
global asm_main
asm_main:
enter 0,0 

; ---- start
mov eax, 0
mov	ecx, [number]		; ECX to zmienna I. i=number
petla_for: 
	cmp	    ecx, 0
	jbe	    koniec_petli	; wychodzimy, gdy i <= 1

	mov edx, 0			;clear before div
	mov eax, [number]	;number / ecx
	mov edi, ecx
	div edi 				; EAX = (EDX:EAX div EDI),
              				; EDX = (EDX:EAX mod EDI)
    ; dump_regs 1
	cmp edx, 0
	jne next

	; divisor

	mov     eax, ecx
	call print_int
	call print_nl
	

	next:
	dec	    ecx		        ; i=i-1
	jmp	    petla_for
koniec_petli:

; --- end

koniec_programu:
mov rax, 0 ; kod zwracany z funkcji
leave
ret


