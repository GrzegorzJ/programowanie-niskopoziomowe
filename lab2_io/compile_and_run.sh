#!/bin/bash
clear
echo "Compile, link and run:" $1
echo "#####################"
nasm -felf64 -o $1.o $1.asm
gcc -o $1 $1.o driver64.o asm64_io.o 
./$1

#sample run command
#./compile_and_run.sh zad1