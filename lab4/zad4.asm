;Proszę napisać podprogram konwertujący liczbę całkowitą na liczbę w systemie szesnastkowym.
;Na wejściu na stosie znajduje się dana liczba całkowita N (i adres powrotu). 
;Po powrocie z funkcji na szczycie stosu powinien się znajdować adres, pod którym znajduje się liczba N. 
;A następnie łańcuch tekstowy reprezentujący liczbę N w systemie szesnastkowym zakończony zerem.
;Uwaga: stos jest kwantowany więc należy odpowiednio uzupełnić tekst spacjami. 
;
;W programie głównym wywołać funkcję dla przykładowych danych i wypisać rezultat np. ams_io,
;odpowiednio sprzątając stos po wywołaniu.
;
%include "asm64_io.inc"

segment .bss

segment .text

segment .data
	ascii_hex db 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 0ah
	num dd 255	 
	num_str db "      ", 0 ; po skonwertowaniu przechowuje rezultat
	num_str_len equ $ - num_str 	

global asm_main
enter 0, 0

asm_main:

mov rax, 0
mov eax, [num]
push rax

mov rax, 0
mov eax, show_result ; adres powrotu
push rax

jmp dec_to_hex

show_result:
	mov rax, rsp ; stack pointer
	call print_string

mov rax, 0
leave 
ret               
	
dec_to_hex:
	pop rsi ; adres powrotu
	pop rax 	
	push rax
	
	;dodajemy do stringa od tylu
	mov ecx, num_str_len
	dec ecx ; ostatnia cyfra 0
	dec ecx ; liczmy od zera
	
	;dzielimy przez 16
	mov edi, 16
	convert_loop:
		cmp eax, 0
		je convert_loop_end
		
		mov edx, 0
		div edi
		
		;pobiera odowiednik hex dla reszty dec 
		mov ebx, [ascii_hex + edx]
		mov [num_str + ecx], bl
		dec ecx
		jmp convert_loop
	convert_loop_end:
	
	mov rax, [num_str]
	push rax
		
	jmp rsi

end_dec_to_hex:

