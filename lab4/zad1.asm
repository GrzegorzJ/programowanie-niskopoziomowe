%include "asm64_io.inc"

segment .bss
    input1 resd 1
    input2 resd 1

segment .text
global asm_main
enter 0, 0

asm_main:
	mov ebx, input1             
	mov ecx, ret1               
	jmp short get_int           
	ret1:

	mov ebx, input2
	mov ecx, $ + 7              
		                         
	jmp short get_int
	
	mov eax, [input1]
	add eax, [input2]
	call print_int

	mov rax, 0
	leave 
	ret               
	
get_int:
	call read_int
	mov [ebx], eax                 
	jmp rcx        
