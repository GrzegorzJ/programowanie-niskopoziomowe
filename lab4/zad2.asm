;Proszę zaimplementować program wypisujący liczbę całkowitą zawartą w rejestrze EAX. Procedura powinna korzystać z
;dzielenia całkowitego, stosu a następnie wypisuje kolejne literki zdejmowane ze stosu.


%include "asm64_io.inc"

segment .bss
    input1 resd 1
    input2 resd 1

segment .text
global asm_main
enter 0, 0

asm_main:

call read_int

mov edi, 10 ; divisors
mov ebx, eax ; 

push 'a' ; end of the stack marker
 
divide_number_loop:
	cmp eax, 0
	jle end_divide_number_loop
	
	;mov edx, 0	
	mov edi, 10	
	div edi

	push rdx
	jmp divide_number_loop
end_divide_number_loop:


print_number_loop:
	pop rax
	cmp eax, 'a'
	je end_print_number_loop

	call print_int
	
	jmp print_number_loop
end_print_number_loop:

call print_nl

mov rax, 0
leave 
ret               
	

