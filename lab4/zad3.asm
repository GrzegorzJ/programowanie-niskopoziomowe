;Proszę zaimplementować funkcję wykonującą sortowanie przez wybieranie.
;Przed wywołaniem funkcji na stosie znajdują się liczba elementów tablicy i wskaźnik na pierwszy element tablicy. 
;Funkcja powinna samodzielnie posprzątać te dane ze stosu (proszę uważać na adres powrotu) bez wykorzystywania polecenia RET.

%include "asm64_io.inc"

segment .bss

segment .data
	elements dd 4, 8, 7, 5, 2
	elements_size dd 5

segment .text
global asm_main
enter 0, 0

asm_main:

;odlozenie na stos elementow tablicy i ich liczby
mov rax, elements
push rax
mov rax, [elements]
push rax
mov rax, [elements_size]
push rax

mov rsi, 0
mov esi, return_address
jmp short sort_elements

return_address:

mov ecx, 0
display_elements:
	cmp ecx, [elements_size]
	je end_display_elements

	mov eax, [elements + ecx * 4]
	call print_int

	inc ecx
	jmp display_elements
end_display_elements:


mov rax, 0
leave 
ret               
	

;for i = 1 to length(A) - 1
;    x = A[i]
;    j = i
;    while j > 0 and A[j-1] > x <=> j <= 0 or A[j-1] <= x
;        A[j] = A[j-1]
;        j = j - 1
;    A[j] = x

sort_elements:
	
	pop rdi ; rozmiar tablicy
	dec edi

	pop rax ; wskaznik na poczatek tablicy
		
	
	mov ecx, 1
	sorting_loop:
		cmp ecx, edi
		je end_sorting_loop
			
		mov ebx, [eax + ecx * 4]; x = A[i]   x->ebx
		mov edx, ecx; j = i  				 j-> edx
		
		inner_sorting_loop:
			cmp edx, 0 ; j <= 0
			jge end_inner_sorting_loop
			
			cmp [eax + edx * 4 - 1], edx
			jle end_inner_sorting_loop 
			
			push rbp ; brak dodadkowego wolnego rejstru
				mov rbp, [eax + edx * 4 - 1]
				mov [eax + edx * 4], rbp
			pop rbp
			dec edx
			
			jmp inner_sorting_loop
		end_inner_sorting_loop:			
		
		mov [eax + edx * 4], ebx
		inc ecx
	end_sorting_loop:

jmp rsi ; powrot do return_address
