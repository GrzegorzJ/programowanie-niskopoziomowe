

section .text
global _start

_start:

	mov rax, 201				;get time
	mov rdi, 0 					;null
	syscall

	mov rdx, 0 					;clear rdx
	mov rsi, 10
	div rsi						;rax = rdx:rax div rsi
								;rdx = rdx:rax mod rsi
	add dl, '0'
	mov [text+7], dl

	mov rdx, 0 					;clear rdx
	mov rsi, 6
	div rsi						;rax = rdx:rax div rsi
								;rdx = rdx:rax mod rsi
	add dl, '0'
	mov [text+6], dl

	mov rdx, 0 					;clear rdx
	mov rsi, 10
	div rsi						;rax = rdx:rax div rsi
								;rdx = rdx:rax mod rsi
	add dl, '0'
	mov [text+4], dl

	mov rdx, 0 					;clear rdx
	mov rsi, 6
	div rsi						;rax = rdx:rax div rsi
								;rdx = rdx:rax mod rsi
	add dl, '0'
	mov [text+3], dl

	mov rdx, 0 					;clear rdx
	inc rax						;timezone shift
	mov rsi, 24					;get hours
	div rsi						;rax = rdx:rax div rsi
								;rdx = rdx:rax mod rsi

	mov rax, rdx 				;take 'mod' result into account
	mov rdx, 0

	mov rsi, 10
	div rsi				
	add dl, '0'
	mov [text+1], dl

	mov rdx, 0 					;clear rdx
	mov rsi, 10
	div rsi						;rax = rdx:rax div rsi
								;rdx = rdx:rax mod rsi
	add dl, '0'
	mov [text+0], dl


	mov rax, 1
	mov rdi, 1
	mov rsi, text
	mov rdx, 8
	syscall

	mov rax, 60
	mov rdi, 0
	syscall

section .data
	text db "XX:XX:XX", 0ah