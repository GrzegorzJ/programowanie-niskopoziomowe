;wersja 64bit
;nasm -felf64 zad1.asm -o zad1.o
;ld zad1.o -o zad1

section .text
global _start

_start:
	mov rax, 1 ; sys write
	mov rdi, 1 ; numer pliku do zapisu
	mov rsi, podaj_imie 
	mov rdx, podaj_imie_dl  
	syscall ; wywolujemy funkcje systemowa

	;wczytywanie imienia
	mov rax, 0
	mov rdi, 0
	mov rsi, imie
	mov rdx, imie_dl
	syscall

	mov rax, 1 ; sys write
	mov rdi, 1 ; numer pliku do zapisu
	mov rsi, hello ; rsi = adres tekstu
	mov rdx, hello_dl  
	syscall ; wywolujemy funkcje systemowa

	mov rax, 60 ; numer funkcji systemowej 60 - exit
	syscall

section .data
	podaj_imie db "Podaj imie", 0ah
	podaj_imie_dl equ $ - podaj_imie
	hello db "##Hello##", 0ah	
	imie db "          ", 0ah ; alokacja można też z resb	
	imie_dl equ $ - imie
	hello_dl equ $ - hello ; hello zadeklarowane przed imie	
