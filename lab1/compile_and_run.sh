#!/bin/bash
clear
echo "Compile, link and run:" $1
echo "#####################"
nasm -felf64 $1.asm -o $1.o
ld $1.o -o $1
./$1

#sample run command
#./compile_and_run.sh zad1