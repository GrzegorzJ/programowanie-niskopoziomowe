section .text
global _start

_start:
	; prosba o podanie pierwzszej liczby
	mov rax, 1 
	mov rdi, 1 
	mov rsi, wpisz_l1 
	mov rdx, wpisz_l1_dl  
	syscall 

	;wczytanie pierwszej liczby liczba
	mov rax, 0
	mov rdi, 0
	mov rsi, liczba1
	mov rdx, 2; bufor dla enter'a
	syscall

	; prosba o podanie drguej liczby 
	mov rax, 1 
	mov rdi, 1 
	mov rsi, wpisz_l2 
	mov rdx, wpisz_l2_dl  
	syscall

	;wczytanie drugiej liczby
	mov rax, 0
	mov rdi, 0
	mov rsi, liczba2
	mov rdx, 2; bufor dla enter'a
	syscall

	;"a" - "0"
	mov rax, [liczba1]
	sub rax, '0'

	;"b" - "0"
	mov rbx, [liczba2]
	sub rbx, '0'

	; a+b = result
	add rax, rbx

	;result + "0"
	add rax, [zero]

	mov [wynik], rax
			
 
	; wypisanie wyniku 
	mov rax, 1 
	mov rdi, 1 
	mov rsi, wynik_to 
	mov rdx, wynik_to_dl  
	syscall
	
	;wypisanie wyniku
	mov rax, 1  
	mov rdi, 1 
	mov rsi, wynik 
	mov rdx, 4
	syscall

	mov rax, 60 ;  60 - exit
	syscall

section .data
	
	wpisz_l1 db "wpisz pierwsza liczbe ", 0xA, 0xD
	wpisz_l1_dl equ $ - wpisz_l1
	wpisz_l2 db "wpisz druga  liczbe ", 0xA, 0xD
	wpisz_l2_dl equ $ - wpisz_l2
	wynik_to db "wynik to : ", 
	wynik_to_dl equ $ - wynik_to	
	zero db "0"
	zero_dl equ $ - zero 

segment .bss
	liczba1 resb 2
	liczba2 resb 2
	wynik resb 4
