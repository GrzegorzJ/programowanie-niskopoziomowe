;nasm -felf32 zad3.asm -o zad3.o
;ld -m elf_i386 zad3.o -o zad3

section .text
global _start

_start:
	;stworzenie pliku o nazwie 'nazwa_pliku'
	mov eax, 8
	mov ebx, nazwa_pliku
	mov ecx, 0777 ; uprawnienia
	int 80h
	
	mov [fd_out], eax ; zapamientanie deskryptora	

	;zapis do pliku
	mov eax, 4
	mov ebx, [fd_out]	
	mov ecx, wpis ; zapis tekst	
	mov edx, wpis_dl	
	int 80h
	
	;zamkniecie pliku
	mov eax, 6
	mov ebx, [fd_out]
	int 80h

	;exit
	mov eax, 1
	int 80h

section .data
	nazwa_pliku db "test_file.txt", 00
	nazwa_pliku_dl equ $ - nazwa_pliku
	wpis db "Grzegorz Jaromin"
	wpis_dl equ $ - wpis

section .bss
	fd_out resb 1
