
BITS 64
section .text 

global iloczyn
iloczyn:

; extern "C" int iloczyn (int n, ...);
; rdi - n
enter 0,0

; RDI, RSI, RDX, RCX, R8, R9

mov rbx, rdx ;trzeba zapamietac rdx bo przy mnozeniu sie nadpisze
mov rax, 1

cmp rdi, 1
jl koniec
mul rsi
dec rdi

cmp rdi, 1
jl koniec
mul rbx
dec rdi

cmp rdi, 1
jl koniec
mul rcx
dec rdi

cmp rdi, 1
jl koniec
mul r8
dec rdi

cmp rdi, 1
jl koniec
mul r9
dec rdi


dec rdi
sumuj:
	cmp rdi, 0
	jl koniec_sumowania
	mov rcx, 0
	mov ecx, [rbp + 16 + 4*rdi] ;tu jest cos zle
	mul ecx

	dec rdi
	jmp sumuj
koniec_sumowania:

koniec:
; zwracamy wynik w rax
leave                           
ret
