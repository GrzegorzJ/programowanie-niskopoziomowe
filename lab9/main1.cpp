// KOMPILACJA - kod źródłowy C w main.c, kod źródłowy ASM w suma.asm
// LINUX :
// jak sa problemy z kompilacja c na linuxie to:
// zainstalowac http://stackoverflow.com/questions/23498237/compile-program-for-32bit-on-64bit-linux-os-causes-fatal-error
// nasm -felf32 suma.asm -o suma.o
// gcc -m32 -o main.o -c main.c
// gcc -m32 main.o suma.o -o suma
// 
// WINDOWS :
// nasm -fcoff suma.asm -o suma.obj
// gcc -o main.obj -c main.c
// gcc main.obj suma.obj -o suma.exe
#include <iostream>

extern "C" int iloczyn (int n, int * tab);

int main()
{
	int tab[] ={1, 2, 3, 4};

  	std::cout << iloczyn(4, tab) << std::endl;

  return 0;
}
/* Wyjście:
 * 14
 * 2
 */ 