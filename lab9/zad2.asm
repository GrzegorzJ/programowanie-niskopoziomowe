
BITS 64
section .text 

global wartosc
 
wartosc:

enter 0,0
; XMM0 - a
; XMM1 - b
; XMM2 - x

mulsd xmm0, xmm2
addsd xmm0, xmm1

; zwrocic w XMM0 = ax+b

leave                           
ret


