
BITS 64
section .text 

global iloczyn
iloczyn:

; extern "C" int iloczyn (int n, int * tab);
; rdi - n
; rsi - *tab
enter 0,0

dec rdi
mov rax, 1
; petla
petla:
	cmp rdi, 0
	jl koniec_petli

	mov rdx, [rsi + 4 * rdi]
	mul rdx

	dec rdi
	jmp petla
koniec_petli:


; zwracamy wynik w rax

leave                           
ret
